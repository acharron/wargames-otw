# Natas

Home page : https://overthewire.org/wargames/natas/

## URLs

Template : 
```
http:// : @natas .natas.labs.overthewire.org/
```

* http://natas0:natas0@natas0.natas.labs.overthewire.org/
* http://natas1:gtVrDuiDfck831PqWsLEZy5gyDz1clto@natas1.natas.labs.overthewire.org/
* http://natas2:ZluruAthQk7Q2MqmDeTiUij2ZvWy2mBi@natas2.natas.labs.overthewire.org/
* http://natas3:sJIJNW6ucpu6HPZ1ZAchaDtwd7oGrD14@natas3.natas.labs.overthewire.org/
* http://natas4:Z9tkRkWmpt9Qr7XrR5jWRkgOU901swEZ@natas4.natas.labs.overthewire.org/
* http://natas5:iX6IOfmpN7AYOQGPwtn3fXpbaJVJcHfq@natas5.natas.labs.overthewire.org/
* http://natas6:aGoY4q2Dc6MgDq4oL4YtoKtyAg9PeHa1@natas6.natas.labs.overthewire.org/
* http://natas7:7z3hEENjQtflzgnT29q7wAvMNfZdh0i9@natas7.natas.labs.overthewire.org/
* http://natas8:DBfUBfqQG69KvJvJ1iAbMoIpwSNQ9bWe@natas8.natas.labs.overthewire.org/
* http://natas9:W0mMhUcRRnG8dcghE4qvk3JA9lGt8nDl@natas9.natas.labs.overthewire.org/
* http://natas10:nOpp1igQAkUzaI1GUUjzn1bFVj7xCNzu@natas10.natas.labs.overthewire.org/
* http://natas11:U82q5TCMMQ9xuFoI3dYX61s7OZD9JKoK@natas11.natas.labs.overthewire.org/


## Explications

(Note : Je change de convention par rapport à Bandit. 
"NatasXX" signifie comment résoudre l'énigme en étant loggé en tant que natasXX)

### Natas0

View source code.

### Natas1

F12 pour ouvrir les Developper Tools

### Natas2

Il y a juste une image d'1 pixel. A continuer...

En fait, il suffit d'aller sur `/files` et voir qu'il y a un fichier txt.

Morale : Verifier les dossiers visibles sur la toile (settings Apache?)

### Natas3

Check `/robots.txt` et voir qu'il existe un dossier `/s3cr3t`

Morale : Cacher un fichier dans robots.txt ne le rend pas invisible

### Natas4

```
Access disallowed.
You are visiting from "http://natas4.natas.labs.overthewire.org/index.php" 
while authorized users should come only from "http://natas5.natas.labs.overthewire.org/" 
```

Ouvrir Dev Tools, tab Network
Edit and Resend la requete pour index.php, en changeant le Request Header Referer

Morale : Pas d'authentification avec juste le Referer


### Natas5

Ouvrir Dev Tools, tab Network

On voit que la reponse a l'header
```
Set-Cookie: loogedin=0
```

2 solutions :

* On fait un Edit and Resend pour avoir en request header `Cookie: loggedin=1`
* On va dans la tab Storage, et on change le Cookie

Morale : On authentifie pas simplement avec un cookie


### Natas6

Voir le source code de la page, et se rendre sur `/includes/secret.inc`

La reponse est un code PHP tout court, avec la variable `$secret` initialisee.


### Natas7

Voir l'indice ou se souvenir que tous les mots de passes sont 
enregistres dans `/etc/natas_webpass/natasXX`

natas7 peut voir son mot de passe et le suivant, il suffit donc de passer en param
```
/index.php?page=/etc/natas_webpass/natas8
```

Morale : Ne pas servir des fichiers directement via PHP


### Natas8

Source code :
```
$encodedSecret = "3d3d516343746d4d6d6c315669563362";

function encodeSecret($secret) {
    return bin2hex(strrev(base64_encode($secret)));
}
```
Il suffit de creer un fichier PHP de test, et ecrire une fonction de decode
```
function decodeSecret($encoded) {
    return base64_decode(strrev(hex2bin($encoded)));
}
```

Morale : Pas d'encodage avec des functions reversibles...


### Natas9

```
if($key != "") {
    passthru("grep -i $key dictionary.txt");
}
```

```
grep -i a /etc/natas_webpass/natas10 dictionary.txt
```


### Natas10

```
grep -i c /etc/natas_webpass/natas11 dictionary.txt
```

